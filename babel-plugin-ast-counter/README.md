# babel-plugin-ast-counter

## Usage

### Via `.babelrc` (Recommended)

**.babelrc**

```json
{
  "plugins": ["./babel-plugin-ast-counter"]
}
```

### Via Node API

```javascript
require('babel').transform('code', {
  plugins: ['./babel-plugin-ast-counter']
});
```
